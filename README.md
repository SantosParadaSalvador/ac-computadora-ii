# ac-computadora-ii
## Computadoras Electrónicas 

```plantuml
@startmindmap
* Computadoras Electrónicas 
** Harvard Mark l
*** Descripcion
**** Una de las computadoras, mas costosas fue la Harvard Mark 1 IBM -1944, durante la segunda guerra mundial, \n para los aliados, y tenia 765000 componentes, 80 km de cable, un eje de 15 m, además de utilizar un motor \n de 5 caballos de fuerza, el pilar de estas computadoras en el rele que es un interruptor, que se controlaba \n eléctricamente.
****  Uso: Uno de los principales usos para los aliados de esta computadora utilizarla en el proyecto manhattan.
**** Características: La Harvard podría realizar las siguientes operaciones aritméticas
***** 1.Esta podia realizar 3 sumas o restas por segundo
***** 2.Multiplicación en 6 segundos
***** 3.División en 15 segundos 
***** 4.Operaciones complejas, en minutos
*** Puntos 
**** Desventajas \n \n La Computadora Harvard tenía 3.500 relés, lo que hacia en fallara un relé al día. \n Otro de los problemas de la Harvard fue el tamaño, combinado que esta despedían \n calor, y esta a su vez podría atraer insectos, donde estos se alojarían, ocasionando \n más problemas, dado a esto se les coloca coloquialmente a los errores como bugs \n en las computadoras.



** Componentes.
*** Válvula termoiónica.
**** Descripción: La válvula termoiónica está formado por un filamento y dos electrodos dentro de un bulbo de cristal, donde \n realiza el proceso de emisión termo iónica, que permite el paso de la corriente en una dirección, posteriormente este se \n  actualizaría agregando un electrodo de control, donde este permite el flujo o detiene la corriente, de esta manera siendo \n una mejor opción viable , pero estos tubos eran frágiles, donde se podrían dañar. 
*** ENIAC.
**** Diseñada y construida por Jhon Mauchly y J.Presper Eckert 
**** Descripción:Esta fue construida en 1946 en la universidad de Pensilvania, además de ser la primera computadora electrónica programable de propósito general
****  Podia realizar 5000 sumas y restas de dígitos por segundo.
**** Esta opero por 10 años, además se calcula y especula que está a realizado más operaciones aritméticas que toda la humanidad
**** Puntos 
***** Desventajas esta era utilizada, hasta la mitad del día por las fallas en los tubos de vacío.
*** Transistor 
**** Creado en los laboratorios bell, por Jhon Bardeen, Walter Brittain y William Shockley
**** Descripción: La ciencia en los transistores es que esta construido de 3 capas de silicio en una forma de sándwich en donde cada capa representa un conector, donde cada \nuno de los conectores son llamados, colector, emisor, base, que tiene como objetivo el comportarse como un interruptor eléctrico
**** Estos están hechos de silícico, en donde se les  proporcionan otros elementos, que le permiten la conducción de la corriente eléctrica, es muy importante el elemento que se le agrega el silicio,  ya que este puede tener exceso o defecto de electrones.
**** Puntos  
***** Cumplen la misma fusión que los tubos de vacío
***** El prototipo de este podía cambiar de estado unas 10 veces, por segundo
***** Los transistores eran solidos y muy resistentes
***** Los componentes, podían volverse más pequeños y prácticos
***** Hoy en día las computadoras tienen transistores de un tamaño de 50 nm
***** Son muy rápidos además de funcionar por muchos años

** Colossus Mark 1 
*** Descripcion
**** Diseñada por el ingeniero tommy flowwers 
**** Esta fue instalada en el reino unido, donde su función era el de decodificar las codificaciones nazis
*** Puntos 
**** Es conocida como la primera computadora programable 
**** Se llegaron a contruir 10 Colossus
**** Tenia 1600 tubos de vacío.
** IBM 
*** Descripcion
**** Fue lanzada en 1957 \n\nLas primeras computadoras, basadas en transistores podían realizar 4.500 sumas, 80 divisiones o multiplicaciones por segundo.
*** Puntos 
**** Esta acerco los computadoras a las oficinas



@endmindmap
```
## Arquitectura Von Neumann y Arquitectura Harvard


```plantuml
@startmindmap
* Arquitectura de las Computadoras 
** Que es la ley de Moore
*** Que es
**** Se entiende como ley de moore a el poder o la velocidad del procesamiento, y el como este \nse duplica cada doce meses y a la cantidad de transistores. 
**** Ley de la electrónica
***** El numero de transistores se duplica cada año.\nCada 18 mese se incrementa la potencia del calculo sin modificar el costo. 
**** Performance 
***** Se incrementa la velocidad del procesador y la capacidad de la memoria.\nLa velocidad de memoria, siempre está detrás de la de la velocidad del procesador. 

** Funcion de las comutadoras 
*** Las primeras computadoras funcionaban conforma a cables y conforme a como se enchufaban. 
**** programacion mediate hardware
***** [Tenía una entrada de datos] -> [funciones logias o aritméticas] -> [decodificar mensaje]
***** El programa era una conjunción referente al enchufe de cables.
*** Actual
**** La programación se hace mediante software, nosotros solo le damos órdenes. 
**** [Datos] > [interprete de instrucciones ]> [señal de control] > [secuencia de funciones aritméticas/lógicas que se van a ejecutar] > [resultado]
** Arquitectura Von Neumann 
*** Esta parte de 3 partes fundamental 
**** La CPU
**** Memoria
**** Modulo de E/S 
*** MODELO.
**** Esta arquitectura fue descrita por el físico matemático John Von Neuman en 1945.\n\n•Los datos y los programas, se pueden almacenar en una misma memoria de lectura-escritura.\n•Los contenidos de esta memoria, acceden indicando su posición sin importar su tipo.\n•Su ejecución es en secuencia, claro si esta indica lo contrario.\n•Tiene una representación Binaria
*** Que contiene la Arquitectura de Von Neuman
**** CPU: esta es la unidad de procesamiento, en la cual contiene una unidad de control, una unidad aritmética lógica y los registros que son una memoria de rápido acceso.\nMemoria Principal: Esta puede almacenar tanto las instrucciones como a los datos.\nSistema de entrada/salida.\nSistema de buses. 
**** Puntos.
***** En la arquitectura de Von Neumann, la separación de la memoria y la CPU acarreo un problema conocido coloquialmente como cuello de botella,\nen el que la cantidad de datos, pasan entre estos 2 elementos, y el tiempo difiere mucho, con lo que la CPU llega a permanecer ociosa.
***** La velocidad puede diferir, esto dependiendo de los diferentes dispositivos, cambiando la velocidad de escritura y de lectura.
***** Es la arquitectura que es utilizada hoy en día. 
*** Buses
**** Que es un bus
***** Un bus es un dispositivo, que permite conectar a dos o mas dispositivos, este funcionando como un Nexus, o mediador, cuando un dispositivo quiera enviar o pedir, algo al mismo tiempo, ya que cuando dos dispositivos \ntrasmitan al mismo tiempo estas, puedan llegar a distorsionarse, y posteriormente perder la información.
**** La comunicación entre los componentes se realiza mediante y a \ntravés de un sistema de buses, en las que se pueden dividir en
***** Buses de datos.
***** Buses de direcciones.
***** Buses de Control.

** Arquitectura Harvard
*** Concepto.
**** La arquitectura Harvard, son las que originalmente se utilizaban los dispositivos de almacenamiento, que físicamente estaban separados para las instrucciones y para los datos.
**** CPU o unidad de procesamiento, en donde contiene una unidad de control, una unidad aritmética lógica y registro. 
**** Memoria principal la cual puede almacenar tanto instrucciones como a los datos.
**** Sistema de Entrada/Salida. 
*** Memorias
**** El poder fabricar memorias con mas rapidez, le pasa un alto precio, por lo que es conveniente tener una memoria igual de rápida, pero mas pequeña, y a esta se le llama cache.
**** Mientras los datos se necesiten estos estarán en el cache, así el rendimiento será mucho mejor.
**** Arquitectura de Harvard solución particular
***** Las instrucciones se almacenan en caches separadas, esto para mejorara el rendimiento, con la pega de tener que dividir a la cantidad de cache.
*** Procesador
**** Esta tiene dos funciones principales.
***** Unidad de control (UC)
****** La unidad UC lee instrucción de la memoria de instrucciones
***** Unidad aritmética y lógica (ALU)
*** Memoria de instrucciones
**** Esta memoria es la encargada de almacenar las instrucciones del programa que debe ejecutar el microcontrolador. 
**** La memoria de instrucciones se implementa utilizando memorias no volátiles.
***** ROM, PROM, EPROM, EEPROM
*** Memoria de datos
**** Es la encargada de almacenar los datos utilizando los programas y estos datos varían continuamente, en donde habitualmente se utiliza SRAM(Memoria RAM estática.)
*** micro controlador
**** Para que se utilizan? 
***** Los micro controladores son utilizados en el desarrollo de producto o de sistemas, con un propósito específico
**** Donde se utilizan? 
***** Estas son utilizadas en los electrodomésticos, telecomunicaciones, automóviles, mouse, impresoras, en el procesamiento \ndel audio y del video, inclusive en la robótica. 


@endmindmap
```
## Basura Electrónica


```plantuml
@startmindmap
* Basura Electrónica
** Que es la RAEE
*** La RAEE (Residuos de aparato eléctricos y electrónicos), también vulgarmente se le puede llamar basura electrónica.
*** Las RAEE están principalmente hechos de compuestos de metal valioso y de circuitos, la basura electrónica, puede clasificar, según sea su composición o que tenga más metales valiosos.
*** De donde procede? 
**** Los RAEE son residuos de los aparatos eléctricos y electrónicos procedentes tanto de hogares particulares como de empresas 
*** Ejemplos de RAEE 
**** Licuadoras
**** Televisores
**** Computadoras 
**** Colsolas 
*** Causas 
**** Los equipos al llegar a quedarse obsoletos y estar descontinuados se convierten en RAEE
**** Los equipos al llegan a dañarse
**** Actualización y creación de equipos mas eficientes. 


** Oro en la basura
*** Puntos
**** Contenido valioso
***** Los elementos de los que estos compuestos, la basura electrónica contiene partes de oro, plata y cobre y cobalto ademas de tener diversos plasticos.
***** El proceso inicia con una recolección de estos, para posteriormente ser clasificados, según sea su categoría, para posteriormente estos ser desarmados y que se les extraiga los compuestos valiosos.
**** Prejuicios 
***** La basura electrónica, no es simplemente basura, gran cantidad de basura es exportada de forma ilegal a países tercermundistas, todo esto para sacar los metales preciosos de los componentes.
****** países tercermundistas
******* China 
******* México 
******* India 
***** Muchas veces la basura electrónica no es reciclada de la manera correcta, sino, que esta llega a manos de personas o compañías, que no llegan a poder reciclar correctamente, además de que sus practicas de reciclaje, producen mucha contaminación.
***** Una de las principales causas de la exportación de basura electrónica, por los metales precio, esto requiere de una fuerte inversión, además de tener su riesgo, ya que la busara electronica despide elementos toxicos al hambiente.
***** Los residuos electrónicos, muchas veces son quemados, además de ser uno de los contaminantes principales en el agua y del aire. 
***** Una gran cantidad de basura electrónica, no llega a las fábricas, y estas terminan en el ambiente, contaminándolo. 
** Reciclaje de desechos Electrónicos
*** Concepto 
**** Los equipos obsoletos que ya no se ocupan en casa, son una de las principales fuentes generadoras de materias primas secundarias
*** La unión europea implemento en el 2016 una nueva ley
**** En la que consistió que cada país recoja 45 toneladas de basura electrónica,  por 100 toneladas de productos electrónicos, puesto a la venta. 
*** Clasificación de materiales de reciclaje
**** De la basura electrónica, se pueden sacar, hierro, silicio, aluminio, oro, plata y cubre además de una gran variedad de plásticos, de esta manera se pueden reducir los costos de la construcción de nuevos sistemas. 
**** Pasos para el reciclaje 
***** El primer paso es la recepción del material.
***** El segundo paso es desarmarlo y clasificarlo según sea su clasificación y sus componentes.
***** El tercer paso es la Limpieza de los componentes 
***** El cuarto paso es la clasificación de estos.
***** El quito paso es la Exportación. 
** Techemet
*** La empresa techemet cumple con las normas necesarias, para el cumplimiento y el uso de sus recursos y personal, para llevar a cabo de forma eficiente, el reciclaje de basura electrónica
*** Esta brinda beneficios económicos a los individuos o empresas, además de ser accesible gracias a los diferentes puntos de recolección de desechos en México. 
*** Las empresas son un intermedio entre los componentes y la reutilización.
** Empresa REMSAN
*** Los dispositivos, tiene una vida útil, esto depende cuando los dispositivos se descomponen o se desactualizan
*** La empresa REMSAN reciben distintos tipos de basura electrónica, en donde estas se desarmar, para posteriormente clasificarla y buscar los compuestos, de interés de la que este compuesta la basura electrónica, como plástico, vidrio y metales
*** Unos de los principales procesos que ha realizado REMSAN en la reutilización de los vidrios de la computadoras y monitores.
*** El porcentaje de materiales que puede llegar a reciclar en REMSAN es  del 90%  
** Empresas desechan equipo 
*** Usualmente las empresas, pueden deshacerse de los dispositivos electrónicos obsoletos, con las personas adecuadas, que se dediquen al reciclaje.
*** Las computadoras, aun que no se puedan utilizar directamente por términos de reciclaje con las empresas, estas pueden ser reutilizadas, con sus partes pueden se utilizados, y o revendidas
*** Los periféricos, que al pasar del tiempo se vuelven obsoletos para las empresas, y de esta manera se convierten en basura electrónica, pero estos aún pueden seguir siendo utilizados
*** Usualmente las empresas se desasen de sus equipos electrónicos, ya descontinuados, dándolos por lotes.
*** Las computadoras, se tiran principalmente por que pasan a hacer obsoletas, además las empresas exigen que los discos duros sean destruidos, con énfasis especial 
** Cementerio de computadoras
*** Reacondicionado: Los dispositivos electrónicos, ya obsoletos, pueden ser utilizados para reacondicionados, para posteriormente ponerlas en funcionamientos, además, estas mismas pueden ser utilizadas, en proyectos, personales 
*** Muchas de las veces los dispositivos obsoletos, ya descontinuados, pueden tener un valor hoy en día para los coleccionistas, si esta aun funciona. 
*** Las consolas son dispositivos electrónicos, que, al pasar el tiempo, esto pueden sufrir o dañarse, o inclusive estos puede llegar a estar descontinuadas, de esta manera estas consolas pueden ser considerados como basura electrónica.
*** Pero muchas de las veces estas pueden tener una segunda vida útil, ya sea sacándole las partes o inclusive reacondicionándolas. 
*** Las grabadoras, son dispositivos análogos, que quedaron descontinuadas, pero estas pueden ser recicladas
*** Las partes de las laptops se pueden utilizar para el reciclaje 

@endmindmap
```
